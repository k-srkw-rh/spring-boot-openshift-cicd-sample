# CICD 環境構築

## 前提

- OpenShift 4.7 以降を利用していること (4.8 で動作確認済み)
- カレントディレクトリが gitlab-runner-settings であること
- OpenShift CLI (oc) で cluster-admin 権限を持つユーザでクラスタにログイン済みであること

## 開発、運用で利用する各プロジェクトを作成(si, pt, production)、CI ツール管理用プロジェクトを作成(gitlab-runner)

```
$ oc new-project si
$ oc new-project pt
$ oc new-project production
$ oc new-project gitlab-runner
```

## CI 実行時のキャッシュのバックエンドとなる MinIO をインストールする

1. OpenShift コンソール上の Operator Hub から MinIO Operator をインストール (Marketplace 版ではなく無印: Red Hat Certified 版を選択)

    ![OperatorHub - MinIO](images/operatorhub-minio.png)

    ![Install Operator - MinIO](images/install-operator-minio-1.png)

    ![Install Operator - MinIO](images/install-operator-minio-2.png)

2. MinIO 上のバケットにアクセスするためのアクセスキー、シークレットアクセスキーを持つ Secret オブジェクトを作成

    ```
    $ oc create secret generic minio-creds-secret \
        --from-literal=accesskey=admin \
        --from-literal=secretkey=adminadmin
    ```

3. MinIO コンソールにアクセスするためのアクセスキー、シークレットアクセスキーを持つ Secret オブジェクトを作成

    ```
    $ oc create secret generic console-secret \
        --from-literal=CONSOLE_PBKDF_PASSPHRASE=admin \
        --from-literal=CONSOLE_PBKDF_SALT=admin \
        --from-literal=CONSOLE_ACCESS_KEY=consoleadmin \
        --from-literal=CONSOLE_SECRET_KEY=adminadmin
    ```

4. MinIO 用 Serviceaccount を作成し、 MinIO テナントのデプロイに必要な SCC を付与

    ```
    $ oc create serviceaccount minio
    $ oc adm policy add-scc-to-user nonroot -z minio
    ```

5. Tenant Custom Resource を作成して MinIO をインストール

    ```
    $ oc apply -f minio-tenant.yaml
    ```

6. MinIO の Tenant Custom Resource のステータスが Initialized になっていること、 Pod が起動していることを確認

    ```
    $ oc get tenants.minio.min.io 
    NAME    STATE         AGE
    minio   Initialized   13m

    $ oc get pod
    NAME                             READY   STATUS    RESTARTS   AGE
    minio-console-64df7d9886-884wp   1/1     Running   0          19s
    minio-console-64df7d9886-qzjt8   1/1     Running   0          19s
    minio-ss-0-0                     1/1     Running   0          11m
    minio-ss-0-1                     1/1     Running   0          11m
    minio-ss-0-2                     1/1     Running   0          11m
    minio-ss-0-3                     1/1     Running   0          11m
    ```

7. oc port-forward で minio-console Pod にアクセスするか、oc expose で Route を作成し MinIO コンソールにアクセスする(ID/Pass は consoleadmin/adminadmin)

    - oc port-forward の場合、以下を実行し localhost:9090 で MinIO コンソールにアクセスできることを確認する

        ```
        $ oc port-forward deployment/minio-console 9090
        Forwarding from 127.0.0.1:9090 -> 9090
        Forwarding from [::1]:9090 -> 9090
        ```

    - Route を作成する場合、以下を実行し HOST/PORT に記載のドメインで MinIO コンソールにアクセスできることを確認する

        ```
        $ oc expose service/minio-console
        route.route.openshift.io/minio-console exposed

        $ oc get route
        NAME            HOST/PORT                                                            PATH   SERVICES        PORT           TERMINATION   WILDCARD
        minio-console   minio-console-gitlab-runner.apps.test.ccd1.sandbox1183.opentlc.com          minio-console   http-console                 None
        ```

    ![Console - MinIO](images/console-minio-1.png)

    ![Console - MinIO](images/console-minio-2.png)

8. MinIO コンソール上から gitlab-runner-cache Bucket を作成する

    ![Create Bucket - MinIO](images/create-bucket-minio-1.png)

    ![Create Bucket - MinIO](images/create-bucket-minio-2.png)

## CI Job を実行する GitLab Runner をインストールする

1. CI 時のイメージビルドやデプロイ用に gitlab-runner, si namespace に対して API オブジェクト (BuildConfig, Deployment など) を作成できる権限を付与した Serviceaccount を作成

    ```
    $ oc create serviceaccount ci-robot
    $ oc adm policy add-role-to-user edit -z ci-robot --rolebinding-name=ci-robot
    $ oc adm policy add-role-to-user edit system:serviceaccount:gitlab-runner:ci-robot --rolebinding-name=ci-robot -n si
    ```

2. CI 時に Serviceaccount の変更を許可する設定を含む GitLab Runner の設定ファイルを ConfigMap として作成

    ```
    $ oc create configmap gitlab-custom-config-toml \
        --from-file config.toml=gitlab-custom-config.toml
    ```

3. OpenShift コンソール上の Operator Hub から gitlab-runner namespace を指定して gitlab-runner operator をインストール (Community 版ではなく無印: Red Hat Certified 版を選択)

    ![OperatorHub - GitLab Runner](images/operatorhub-gitlab-runner.png)

    ![Install Operator - GitLab Runner](images/install-operator-gitlab-runner-1.png)

    ![Install Operator - GitLab Runner](images/install-operator-gitlab-runner-2.png)

4. Runner 登録のための Token を持つ Secret 作成(Runner Registration Token は GitLab Project > Settings > CICD > Runners > Specific runners から確認)

    ```
    $ oc create secret generic gitlab-runner-secret \
        --from-literal=runner-registration-token=<Runner Registration Token>
    ```

5. Runner Custom Resource を作成して GitLab Runner をインストール

    ```
    oc apply -f gitlab-runner.yaml
    ```

6. GitLab Runner の Runner Resource が Phase: Running になっていること、 Pod が起動していることを確認

    ```
    $ oc get runners.apps.gitlab.com \
        -ocustom-columns=PHASE:.status.phase,REGISTRATION:.status.registration    
    PHASE     REGISTRATION
    Running   succeeded

    $ oc get pod
    NAME                                                READY   STATUS    RESTARTS   AGE
    gitlab-runner-controller-manager-58db774dfd-zw6wj   2/2     Running   0          34h
    gitlab-runner-runner-f5947798b-d6s7x                1/1     Running   0          30h
    ```

7. GitLab Project > Settings > CICD > Runners > Specific runners から GitLab Runner が登録されていることを確認

    ![Install Operator - GitLab Runner](images/registration-gitlab-runner.png)

## SonarQube をインストールする

1. SonarQube Community が公開している Helm Chart から SonarQube をインストールする

    ```
    $ git clone https://github.com/SonarSource/helm-chart-sonarqube.git
    $ helm dependency update ./helm-chart-sonarqube/charts/sonarqube/
    $ helm upgrade --install -f sonarqube-values.yaml -n gitlab-runner sonarqube ./helm-chart-sonarqube/charts/sonarqube/
    $ oc adm policy add-scc-to-user sonarqube-privileged-scc -z sonarqube
    ```

2. Pod が起動していることを確認

    ```
    $ oc get pod -l release=sonarqube
    NAME                     READY   STATUS    RESTARTS   AGE
    sonarqube-postgresql-0   1/1     Running   0          5h38m
    sonarqube-sonarqube-0    1/1     Running   1          5h37m
    ```

3. Route 経由で SonarQube コンソールにアクセスする(ID/Pass は admin/admin)。初期パスワードの変更を求められるので adminadmin に変更する

    ```
    $ oc expose service/sonarqube-sonarqube
    route.route.openshift.io/sonarqube-sonarqube exposed
    
    $ oc get route sonarqube-sonarqube 
    NAME                  HOST/PORT                                                                  PATH   SERVICES              PORT   TERMINATION   WILDCARD
    sonarqube-sonarqube   sonarqube-sonarqube-gitlab-runner.apps.test.ccd1.sandbox1183.opentlc.com          sonarqube-sonarqube   http                 None
    ```

![Console - SonarQube](images/console-sonarqube-1.png)

![Console - SonarQube](images/console-sonarqube-2.png)

![Console - SonarQube](images/console-sonarqube-3.png)

## OpenShift GitOps インストール方法

1. OpenShift コンソール上の Operator Hub から OpenShift GitOps operator をインストール

    ![OperatorHub - GitOps](images/operatorhub-gitops.png)

    ![Install Operator - GitOps](images/install-operator-gitops-1.png)

    ![Install Operator - GitOps](images/install-operator-gitops-2.png)

2. openshift-gitops Namespace に GitOps 関連の Pod が起動していることを確認する

    ```
    $ oc get pod -n openshift-gitops
    NAME                                                          READY   STATUS    RESTARTS   AGE
    cluster-78779b6d4c-cqhzw                                      1/1     Running   0          62m
    kam-6764ccc9c-49d4z                                           1/1     Running   0          62m
    openshift-gitops-application-controller-0                     1/1     Running   0          62m
    openshift-gitops-applicationset-controller-5d9f9998f8-gmjjc   1/1     Running   0          62m
    openshift-gitops-redis-7867d74fb4-9wgww                       1/1     Running   0          62m
    openshift-gitops-repo-server-579776b7d6-tvl8s                 1/1     Running   0          62m
    openshift-gitops-server-84fcb8547c-hrtzp                      1/1     Running   0          62m
    ```

3. ArgoCD がデプロイ対象の Project にオブジェクトを作成できる権限を与えておく

    ```
    $ oc adm policy add-role-to-user edit system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n pt
    $ oc adm policy add-role-to-user edit system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller -n production
    ```

4. ArgoCD のコンソールにアクセスするための初期パスワードを Secret から確認する

    ```
    # /tmp 配下に admin.password ファイルが作成され、ファイル内でパスワードを確認できる
    $ oc extract secret/openshift-gitops-cluster --keys=admin.password --to=/tmp -n openshift-gitops
    /tmp/admin.password
    ```

5. 以下を実行し HOST/PORT に記載のドメインで ArgoCD のコンソールにアクセスする(ID/Pass は admin/<直前の手順で確認したパスワード>)。

    ```
    $ oc get route openshift-gitops-server -n openshift-gitops
    NAME                      HOST/PORT                                                                         PATH   SERVICES                  PORT    TERMINATION            WILDCARD
    openshift-gitops-server   openshift-gitops-server-openshift-gitops.apps.test.ccd1.sandbox1183.opentlc.com          openshift-gitops-server   https   passthrough/Redirect   None
    ```

    ![Console - ArgoCD](images/console-argocd-1.png)

    ![Console - ArgoCD](images/console-argocd-2.png)

6. [+NEW APP] を選択し以下のように GitOps の対象となるソース Git リポジトリとデプロイ先 Namespace を指定し登録する。PT 環境と本番環境をそれぞれ設定する。

    | General<br>Application Name                  | <br>SYNC POLICY | SOURCE<br>Repository URL                                           | <br>Revision | <br>Path                      | DESTINATION<br>Cluster URL     | <br>Namespace |
    |----------------------------------------------|-----------------|--------------------------------------------------------------------|--------------|-------------------------------|--------------------------------|---------------|
    | pt-spring-boot-openshift-cicd-sample         | Automatic       | https://gitlab.com/k-srkw-rh/spring-boot-openshift-cicd-sample.git | pt           | manifests/overlays/pt         | https://kubernetes.default.svc | pt            |
    | production-spring-boot-openshift-cicd-sample | Automatic       | https://gitlab.com/k-srkw-rh/spring-boot-openshift-cicd-sample.git | production   | manifests/overlays/production | https://kubernetes.default.svc | production    |

    ![Create App - ArgoCD](images/create-app-argocd.png)

7. 設定後しばらくしてそれぞれのアプリケーションの Status が Healthy, Synced になることを確認する

    ![Sync Apps - ArgoCD](images/sync-apps-argocd-1.png)

    ![Sync Apps - ArgoCD](images/sync-apps-argocd-2.png)

8. 実際の Namespace 上にアプリケーションがデプロイされていることを確認する。

    ```
    $ oc get pod -n pt
    NAME                                                 READY   STATUS    RESTARTS   AGE
    spring-boot-openshift-cicd-sample-5fd78fb556-gt86t   1/1     Running   0          79s

    $ oc get pod -n production 
    NAME                                                READY   STATUS    RESTARTS   AGE
    spring-boot-openshift-cicd-sample-5b8dcff78-lkbmg   1/1     Running   0          7m55s
    ```

## GitLab Project 上での CI Pipeline の実行準備

1. GitLab Project > Settings > CICD > Variables で SonarQube の ID/Pass URL を GitLab の Variable として登録する

    | Type | Key | Value | Protected | Masked | Environments |
    |------|-----|-------|-----------|--------|--------------|
    | Variable | SONAR_URL | SonarQube URL | x | x | All (default) |
    | Variable | SONAR_USER | SonarQube ユーザ名 | x | x | All (default) |
    | Variable | SONAR_PWD | SonarQube パスワード | x | ✔️ | All (default) |

2. GitLab Registry にコンテナイメージを Push するときに使用する Deploy Token を作成する

    | Name | Scopes |
    |------|-----|
    | gitlab-registry-key | read_registry, write_registry |

3. GitLab Registry にコンテナイメージを Push するときに使用する認証情報を持つ Secret を作成する

    ```
    $ oc create secret docker-registry gitlab-registry-key \
        --docker-server=registry.gitlab.com \
        --docker-username=<deploy token username>> \
        --docker-password=<deploy token> \
        --docker-email=<email>
    ```